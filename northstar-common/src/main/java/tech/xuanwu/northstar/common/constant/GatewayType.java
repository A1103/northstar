package tech.xuanwu.northstar.common.constant;


public enum GatewayType {
	/**
	 * 本地模拟
	 */
	SIM,
	/**
	 * CTP生产
	 */
	CTP,
	/**
	 * CTP仿真
	 */
	CTP_SIM,
	
	IB;
}
